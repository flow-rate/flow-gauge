import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'flow'
  },
  {
    path: 'flow',
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'test'
      },
      {
        path: 'test',
        loadChildren: () => import('./edit-test/edit-test.module').then(m => m.EditTestModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
