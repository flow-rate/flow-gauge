import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditTestRoutingModule } from './edit-test-routing.module';
import { EditTestComponent } from './edit-test.component';

import { TestFormModule } from './test-form/test-form.module';

@NgModule({
  declarations: [EditTestComponent],
  imports: [
    CommonModule,
    EditTestRoutingModule,
    TestFormModule
  ]
})
export class EditTestModule { }
