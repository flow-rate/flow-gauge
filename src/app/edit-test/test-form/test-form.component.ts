import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TestRun } from 'src/app/types';

@Component({
  selector: 'app-test-form',
  templateUrl: './test-form.component.html',
  styleUrls: ['./test-form.component.scss']
})
export class TestFormComponent implements OnInit {
  @Input() public test: TestRun;
  @Output() public updateTest = new EventEmitter<TestRun>();

  constructor() { }

  ngOnInit() {
    console.log('test', this.test)
  }

  public onSubmit(event): void {
    console.log(this.test);
  }
}
