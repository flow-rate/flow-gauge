import { Component, OnInit } from '@angular/core';
import { TestRun } from '../types';

@Component({
  selector: 'app-edit-test',
  templateUrl: './edit-test.component.html',
  styleUrls: ['./edit-test.component.scss']
})
export class EditTestComponent implements OnInit {
  public test: TestRun;

  constructor() { }

  ngOnInit() {
    this.test = {
      reportName: 'New Test',
      entryUrl: '',
      entryWaitTime: 0,
      steps: [
        {
          actionSelector: '',
          actionType: 'click',
          stepWaitTime: 0
        }
      ]
    };
  }

}
