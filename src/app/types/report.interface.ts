export interface FlowEventBase {
  type: string;
  timestamp: number;
}

export interface VisualUpdateEvent extends FlowEventBase {
  type: 'visualUpdate';
  beforeSnapshot: string;
  afterSnapshot: string;
  difference: number;
}

export interface UserInteractionEvent extends FlowEventBase {
  type: 'userInteraction';
  interactionType: 'click';
}

export type FlowEvent = VisualUpdateEvent | UserInteractionEvent;

export interface RelativeFlowEvent<T> {
  event: T;
  deltaTime: number;
}

export interface TraceReport {
  events: FlowEvent[];
  entryEvent: UserInteractionEvent;
  firstPaint: RelativeFlowEvent<VisualUpdateEvent>;
  finalPaint: RelativeFlowEvent<VisualUpdateEvent>;
}