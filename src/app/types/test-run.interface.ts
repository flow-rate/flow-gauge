
export interface TestRun {
  reportName: string;
  entryUrl: string;
  entryWaitTime?: number;
  steps: Array<TestRunStep>;
}

export interface TestRunStep {
  actionSelector: string;
  actionType: 'click';
  stepWaitTime: number;
}
